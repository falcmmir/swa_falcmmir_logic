FROM maven:latest

COPY . .
RUN mvn clean install -DskipTests

#Copy integratino tests
COPY resources/integration_tests.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/integration_tests.sh

RUN chmod +x init.sh
CMD ./init.sh