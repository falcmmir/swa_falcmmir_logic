package swa.calendar.swa_falcmmir_logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import swa.calendar.swa_falcmmir_logic.repositories.GameSessionRepository;
import swa.calendar.swa_falcmmir_logic.repositories.SessionTimeRepository;
import swa.calendar.swa_falcmmir_logic.repositories.VoteRepository;


@SpringBootTest
public abstract class BaseCT {

    @Autowired
    private GameSessionRepository gameSessionRepository;

    @Autowired
    private SessionTimeRepository sessionTimeRepository;

    @Autowired
    private VoteRepository voteRepository;
}