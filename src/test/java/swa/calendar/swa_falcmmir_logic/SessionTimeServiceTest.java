package swa.calendar.swa_falcmmir_logic;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import swa.calendar.swa_falcmmir_logic.repositories.SessionTimeRepository;
import swa.calendar.swa_falcmmir_logic.services.SessionTimeService;
import swa.calendar.swa_falcmmir_logic.input_types.SessionTimeInput;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
public class SessionTimeServiceTest extends BaseCT {

    @Autowired
    private SessionTimeService sessionTimeService;
    @Autowired
    private SessionTimeRepository sessionTimeRepository;

    @Test
    public void testCreateSessionTime() {
        // Arrange
        Long gameSessionId = 2L; // Assuming this game session exists from data.sql
        SessionTimeInput sessionTimeInput = new SessionTimeInput();
        sessionTimeInput.setDatetimeFrom(LocalDateTime.of(2024, 1, 3, 10, 0));
        sessionTimeInput.setDatetimeTo(LocalDateTime.of(2024, 1, 3, 11, 0));
        sessionTimeInput.setNotified(false);

        // Act & Assert
        assertDoesNotThrow(() -> {
            SessionTimeEntity sessionTimeEntity = sessionTimeService.createSessionTime(gameSessionId, sessionTimeInput);
            assertNotNull(sessionTimeEntity);
            assertEquals(gameSessionId, sessionTimeEntity.getGameSession().getId());
            assertEquals(sessionTimeInput.getDatetimeFrom(), sessionTimeEntity.getDatetimeFrom());
            assertEquals(sessionTimeInput.getDatetimeTo(), sessionTimeEntity.getDatetimeTo());
            assertEquals(sessionTimeInput.getNotified(), sessionTimeEntity.isNotified());
        });
    }

    @Test
    public void testGetSessionTimeById() {
        // Arrange
        Long gameSessionId = 1L; // Assuming this game session time exists from data.sql
        Long sessionTimeId = 1L; // Assuming this session time exists from data.sql

        // Act & Assert
        assertDoesNotThrow(() -> {
            SessionTimeEntity sessionTimeEntity = sessionTimeService.getSessionTimeById(gameSessionId, sessionTimeId);
            assertNotNull(sessionTimeEntity);
            assertEquals(sessionTimeId, sessionTimeEntity.getId());
        });
    }

    @Test
    public void testUpdateSessionTime() {
        // Arrange
        Long gameSessionId = 1L; // Assuming this game session time exists from data.sql
        Long sessionTimeId = 1L; // Assuming this session time exists from data.sql
        SessionTimeInput sessionTimeInput = new SessionTimeInput();
        sessionTimeInput.setDatetimeFrom(LocalDateTime.of(2024, 1, 1, 12, 0));
        sessionTimeInput.setDatetimeTo(LocalDateTime.of(2024, 1, 1, 13, 0));
        sessionTimeInput.setNotified(true);

        // Act
        SessionTimeEntity sessionTimeEntity = sessionTimeService.updateSessionTime(gameSessionId, sessionTimeId, sessionTimeInput);

        // Assert
        assertNotNull(sessionTimeEntity);
        assertEquals(sessionTimeId, sessionTimeEntity.getId());
        assertEquals(sessionTimeInput.getDatetimeFrom(), sessionTimeEntity.getDatetimeFrom());
        assertEquals(sessionTimeInput.getDatetimeTo(), sessionTimeEntity.getDatetimeTo());
        assertEquals(sessionTimeInput.getNotified(), sessionTimeEntity.isNotified());
    }

    @Test
    public void testDeleteSessionTime() {
        // Arrange
        Long gameSessionId = 1L; // Assuming this game session time exists from data.sql
        Long sessionTimeId = 1L; // Assuming this session time exists from data.sql

        // Act
        sessionTimeService.deleteSessionTime(gameSessionId, sessionTimeId);

        // Assert
        Optional<SessionTimeEntity> optionalSessionTime = sessionTimeRepository.findById(sessionTimeId);
        assertFalse(optionalSessionTime.isPresent());
    }
}

