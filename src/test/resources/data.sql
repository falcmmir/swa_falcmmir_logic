-- Insert initial data into game_session table
INSERT INTO game_session (name, description, min_players, max_players, single, creator_id) VALUES
('Test Session 1', 'Description for Test Session 1', 2, 4, true, 1),
('Test Session 2', 'Description for Test Session 2', 3, 5, false, 2);

-- Insert initial data into session_time table
INSERT INTO session_time (game_session_id, datetime_from, datetime_to, notified) VALUES
(1, '2024-01-01 10:00:00', '2024-01-01 11:00:00', false),
(1, '2024-01-02 10:00:00', '2024-01-02 11:00:00', true);

-- Insert initial data into vote table
INSERT INTO vote (creator_id, session_time_id, note, decision) VALUES
(1, 1, 'First vote note', 'YES'),
(2, 1, 'Second vote note', 'NO'),
(3, 2, 'Third vote note', 'MAYBE');
