-- Create game_session table
CREATE TABLE IF NOT EXISTS game_session (
                              id BIGINT AUTO_INCREMENT PRIMARY KEY,
                              name VARCHAR(255) NOT NULL,
                              description TEXT,
                              min_players INT NOT NULL,
                              max_players INT NOT NULL,
                              single BOOLEAN NOT NULL DEFAULT TRUE,
                              creator_id BIGINT NOT NULL
);

-- Create session_time table
CREATE TABLE IF NOT EXISTS session_time (
                              id BIGINT AUTO_INCREMENT PRIMARY KEY,
                              game_session_id BIGINT NOT NULL,
                              datetime_from DATETIME NOT NULL,
                              datetime_to DATETIME NOT NULL,
                              notified BOOLEAN NOT NULL DEFAULT FALSE,
                              FOREIGN KEY (game_session_id) REFERENCES game_session(id) ON DELETE CASCADE
);

-- Create vote table with ON DELETE CASCADE
CREATE TABLE IF NOT EXISTS vote (
                      id BIGINT AUTO_INCREMENT PRIMARY KEY,
                      creator_id BIGINT NOT NULL,
                      session_time_id BIGINT NOT NULL,
                      note TEXT,
                      decision ENUM('YES', 'NO', 'MAYBE') NOT NULL,
                      FOREIGN KEY (session_time_id) REFERENCES session_time(id) ON DELETE CASCADE
);