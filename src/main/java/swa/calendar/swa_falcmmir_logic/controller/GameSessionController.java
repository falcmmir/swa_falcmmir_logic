package swa.calendar.swa_falcmmir_logic.controller;

import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import swa.calendar.swa_falcmmir_logic.context.UserContext;
import swa.calendar.swa_falcmmir_logic.input_types.GameSessionInput;
import swa.calendar.swa_falcmmir_logic.local_entities.GameSessionEntity;
import swa.calendar.swa_falcmmir_logic.security.TokenRequired;
import swa.calendar.swa_falcmmir_logic.services.GameSessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("/game-sessions")
public class GameSessionController {
    private static final Logger logger = LoggerFactory.getLogger(GameSessionController.class);
    private final GameSessionService gameSessionService;

    @Autowired
    public GameSessionController(GameSessionService gameSessionService) {
        this.gameSessionService = gameSessionService;
    }

    @TokenRequired
    @PostMapping
    public ResponseEntity<GameSessionEntity> createGameSession(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @RequestBody GameSessionInput gameSessionInput
    ) {
        logger.info("Received request to create a new game session: {}", gameSessionInput);
        GameSessionEntity createdGameSession = gameSessionService.createGameSession(gameSessionInput, UserContext.getUserId());
        logger.info("Created new game session with ID: {}", createdGameSession.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdGameSession);
    }

    @TokenRequired
    @PutMapping("/{id}")
    public ResponseEntity<GameSessionEntity> updateGameSession(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long id,
            @RequestBody GameSessionInput gameSessionInput) {
        logger.info("Received request to update game session with ID: {}", id);
        GameSessionEntity updatedSession = gameSessionService.updateGameSession(id, gameSessionInput, UserContext.getUserId());
        logger.info("Updated game session with ID: {}", updatedSession.getId());
        return ResponseEntity.ok(updatedSession);
    }

    @TokenRequired
    @GetMapping
    public List<GameSessionEntity> getAllGameSessions(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token
    ) {
        logger.info("Received request to fetch all game sessions");
        return gameSessionService.getAllGameSessions();
    }

    @TokenRequired
    @GetMapping("/{id}")
    public ResponseEntity<GameSessionEntity> getGameSession(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long id
    ) {
        logger.info("Received request to fetch game session with ID: {}", id);
        GameSessionEntity gameSession = gameSessionService.getGameSessionById(id);
        logger.info("Fetched game session with ID: {}", gameSession.getId());
        return ResponseEntity.ok(gameSession);
    }

    @TokenRequired
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGameSession(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long id
    ) {
        logger.info("Received request to delete game session with ID: {}", id);
        gameSessionService.deleteGameSession(id);
        logger.info("Deleted game session with ID: {}", id);
        return ResponseEntity.noContent().build();
    }
}