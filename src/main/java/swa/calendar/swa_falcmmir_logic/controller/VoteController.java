package swa.calendar.swa_falcmmir_logic.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import swa.calendar.swa_falcmmir_logic.context.UserContext;
import swa.calendar.swa_falcmmir_logic.input_types.VoteInput;
import swa.calendar.swa_falcmmir_logic.local_entities.VoteEntity;
import swa.calendar.swa_falcmmir_logic.security.TokenRequired;
import swa.calendar.swa_falcmmir_logic.services.VoteService;

import java.util.List;

@RestController
@RequestMapping("/game-sessions/{gameSessionId}/session-times/{sessionTimeId}/votes")
public class VoteController {
    private static final Logger logger = LoggerFactory.getLogger(VoteController.class);

    private final VoteService voteService;

    @Autowired
    public VoteController(VoteService voteService) {
        this.voteService = voteService;
    }

    @TokenRequired
    @PostMapping
    public ResponseEntity<VoteEntity> createVote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId,
            @RequestBody VoteInput voteInput
    ) {
        logger.info("Received request to create vote for game session ID: {} and session time ID: {}", gameSessionId, sessionTimeId);
        VoteEntity createdVote = voteService.createVote(gameSessionId, sessionTimeId, voteInput, UserContext.getUserId());
        logger.info("Created vote with ID: {} for game session ID: {} and session time ID: {}", createdVote.getId(), gameSessionId, sessionTimeId);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdVote);
    }

    @TokenRequired
    @PutMapping("/{voteId}")
    public ResponseEntity<VoteEntity> updateVote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId,
            @PathVariable Long voteId,
            @RequestBody VoteInput voteInput
    ) {
        logger.info("Received request to update vote with ID: {} for game session ID: {} and session time ID: {}", voteId, gameSessionId, sessionTimeId);
        VoteEntity updatedVoteEntity = voteService.updateVote(gameSessionId, sessionTimeId, voteId, voteInput, UserContext.getUserId());
        logger.info("Updated vote with ID: {} for game session ID: {} and session time ID: {}", updatedVoteEntity.getId(), gameSessionId, sessionTimeId);
        return ResponseEntity.ok(updatedVoteEntity);
    }

    @TokenRequired
    @GetMapping
    public List<VoteEntity> getAllVotes(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId
    ) {
        logger.info("Received request to fetch all votes for game session ID: {} and session time ID: {}", gameSessionId, sessionTimeId);
        List<VoteEntity> votes = voteService.getAllVotes(gameSessionId, sessionTimeId);
        logger.info("Fetched {} votes for game session ID: {} and session time ID: {}", votes.size(), gameSessionId, sessionTimeId);
        return votes;
    }

    @TokenRequired
    @GetMapping("/{voteId}")
    public ResponseEntity<VoteEntity> getVote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId,
            @PathVariable Long voteId
    ) {
        logger.info("Received request to fetch vote with ID: {} for game session ID: {} and session time ID: {}", voteId, gameSessionId, sessionTimeId);
        VoteEntity vote = voteService.getVoteById(gameSessionId, sessionTimeId, voteId);
        logger.info("Fetched vote with ID: {} for game session ID: {} and session time ID: {}", vote.getId(), gameSessionId, sessionTimeId);
        return ResponseEntity.ok(vote);
    }

    @TokenRequired
    @DeleteMapping("/{voteId}")
    public ResponseEntity<Void> deleteVote(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId,
            @PathVariable Long voteId
    ) {
        logger.info("Received request to delete vote with ID: {} for game session ID: {} and session time ID: {}", voteId, gameSessionId, sessionTimeId);
        voteService.deleteVote(gameSessionId, sessionTimeId, voteId);
        logger.info("Deleted vote with ID: {} for game session ID: {} and session time ID: {}", voteId, gameSessionId, sessionTimeId);
        return ResponseEntity.noContent().build();
    }
}
