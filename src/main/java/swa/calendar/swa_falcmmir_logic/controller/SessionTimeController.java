package swa.calendar.swa_falcmmir_logic.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import swa.calendar.swa_falcmmir_logic.input_types.SessionTimeInput;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;
import swa.calendar.swa_falcmmir_logic.security.TokenRequired;
import swa.calendar.swa_falcmmir_logic.services.SessionTimeService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/game-sessions/{gameSessionId}/session-times")
public class SessionTimeController {
    private static final Logger logger = LoggerFactory.getLogger(SessionTimeController.class);
    private final SessionTimeService sessionTimeService;

    @Autowired
    public SessionTimeController(SessionTimeService sessionTimeService) {
        this.sessionTimeService = sessionTimeService;
    }

    @TokenRequired
    @PostMapping
    public ResponseEntity<SessionTimeEntity> createSessionTime(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @RequestBody SessionTimeInput sessionTimeInput) {
        logger.info("Received request to create a new session time: {}", sessionTimeInput);
        SessionTimeEntity createdSessionTime = sessionTimeService.createSessionTime(gameSessionId, sessionTimeInput);
        logger.info("Created new session time with ID: {}", createdSessionTime.getId());
        return ResponseEntity.status(HttpStatus.CREATED).body(createdSessionTime);
    }

    @TokenRequired
    @PutMapping("/{sessionTimeId}")
    public ResponseEntity<SessionTimeEntity> updateSessionTime(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId,
            @RequestBody SessionTimeInput sessionTimeInput) {
        logger.info("Received request to update session time with ID: {} for game session ID: {}", sessionTimeId, gameSessionId);
        SessionTimeEntity updatedSession = sessionTimeService.updateSessionTime(gameSessionId, sessionTimeId, sessionTimeInput);
        logger.info("Updated session time with ID: {} for game session ID: {}", updatedSession.getId(), gameSessionId);
        return ResponseEntity.ok(updatedSession);
    }

    @TokenRequired
    @GetMapping
    public ResponseEntity<List<SessionTimeEntity>> getAllSessionTimesForGameSession(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId
    ) {
        logger.info("Received request to fetch all session times for game session ID: {}", gameSessionId);
        List<SessionTimeEntity> sessionTimes = sessionTimeService.getAllSessionTimesForGameSession(gameSessionId);
        logger.info("Fetched {} session times for game session ID: {}", sessionTimes.size(), gameSessionId);
        return ResponseEntity.ok(sessionTimes);
    }

    @TokenRequired
    @GetMapping("/{sessionTimeId}")
    public ResponseEntity<SessionTimeEntity> getSessionTimeById(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId
    ) {
        logger.info("Received request to fetch session time with ID: {} for game session ID: {}", sessionTimeId, gameSessionId);
        SessionTimeEntity sessionTime = sessionTimeService.getSessionTimeById(gameSessionId, sessionTimeId);
        logger.info("Fetched session time with ID: {} for game session ID: {}", sessionTime.getId(), gameSessionId);
        return ResponseEntity.ok(sessionTime);
    }

    @TokenRequired
    @DeleteMapping("/{sessionTimeId}")
    public ResponseEntity<Void> deleteSessionTime(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @PathVariable Long sessionTimeId
    ) {
        logger.info("Received request to delete session time with ID: {} for game session ID: {}", sessionTimeId, gameSessionId);
        sessionTimeService.deleteSessionTime(gameSessionId, sessionTimeId);
        logger.info("Deleted session time with ID: {} for game session ID: {}", sessionTimeId, gameSessionId);
        return ResponseEntity.noContent().build();
    }

    @TokenRequired
    @GetMapping("/filter")
    public ResponseEntity<List<SessionTimeEntity>> filterSessionTimes(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String token,
            @PathVariable Long gameSessionId,
            @RequestParam(name = "datetimeFrom", required = false) LocalDateTime datetimeFrom,
            @RequestParam(name = "datetimeTo", required = false) LocalDateTime datetimeTo,
            @RequestParam(name = "notified", required = false) Boolean notified
    ) {
        logger.info("Received request to filter session times for game session ID: {}", gameSessionId);
        SessionTimeInput sessionTimeInput = new SessionTimeInput();
        sessionTimeInput.setDatetimeFrom(datetimeFrom);
        sessionTimeInput.setDatetimeTo(datetimeTo);
        sessionTimeInput.setNotified(notified);
        List<SessionTimeEntity> sessionTimes = sessionTimeService.filterSessionTimes(gameSessionId, sessionTimeInput);
        logger.info("Filtered session times for game session ID: {}", gameSessionId);
        return ResponseEntity.ok(sessionTimes);
    }
}
