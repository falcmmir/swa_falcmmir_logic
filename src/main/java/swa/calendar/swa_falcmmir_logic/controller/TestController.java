package swa.calendar.swa_falcmmir_logic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import swa.calendar.swa_falcmmir_logic.local_entities.GameSessionEntity;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;
import swa.calendar.swa_falcmmir_logic.local_entities.VoteEntity;
import swa.calendar.swa_falcmmir_logic.services.GameSessionService;
import swa.calendar.swa_falcmmir_logic.services.SessionTimeService;
import swa.calendar.swa_falcmmir_logic.services.TestService;
import swa.calendar.swa_falcmmir_logic.services.VoteService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/test")
public class TestController {

    private final GameSessionService gameSessionService;
    private final SessionTimeService sessionTimeService;
    private final VoteService voteService;
    private final TestService testService;

    @Autowired
    public TestController(
            TestService testService,
            GameSessionService gameSessionService,
            SessionTimeService sessionTimeService,
            VoteService voteService
    ) {
        this.testService = testService;
        this.gameSessionService = gameSessionService;
        this.sessionTimeService = sessionTimeService;
        this.voteService = voteService;
    }

    @GetMapping("/data")
    public Map<String, Object> getAllData() {
        Map<String, Object> data = new HashMap<>();

        List<GameSessionEntity> gameSessions = gameSessionService.getAllGameSessions();
        List<SessionTimeEntity> sessionTimes = sessionTimeService.getAllSessionTimesForGameSession(1L);
        List<VoteEntity> votes = voteService.getAllVotes(1L, 1L);

        data.put("gameSessions", gameSessions);
        data.put("sessionTimes", sessionTimes);
        data.put("votes", votes);

        return data;
    }

    @GetMapping("/{id}")
    public GameSessionEntity getUserById(@PathVariable Long id) {
        return testService.getGameSessionById(id);
    }

    @GetMapping("/notification")
    public Map<String, String> getNotificationHealth() {
        return testService.getNotificationHealth();
    }

    @GetMapping("/authentication")
    public Map<String, String> getAuthenticationHealth() {
        return testService.getAuthenticationHealth();
    }
}
