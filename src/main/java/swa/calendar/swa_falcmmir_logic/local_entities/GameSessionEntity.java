package swa.calendar.swa_falcmmir_logic.local_entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import swa.calendar.swa_falcmmir_logic.input_types.GameSessionInput;

@Entity
@Table(name = "game_session")
@Getter
@Setter
public class GameSessionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String description;

    private int minPlayers;

    private int maxPlayers;

    private boolean single;

    private long creatorId;

    public void setFromInput(GameSessionInput gameSessionInput) {
        if (gameSessionInput.getName() != null) {
            this.name = gameSessionInput.getName();
        }
        if (gameSessionInput.getDescription() != null) {
            this.description = gameSessionInput.getDescription();
        }
        if (gameSessionInput.getMinPlayers() != null) {
            this.minPlayers = gameSessionInput.getMinPlayers();
        }
        if (gameSessionInput.getMaxPlayers() != null) {
            this.maxPlayers = gameSessionInput.getMaxPlayers();
        }
        if (gameSessionInput.getSingle() != null) {
            this.single = gameSessionInput.getSingle();
        }
    }
}
