package swa.calendar.swa_falcmmir_logic.local_entities;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import swa.calendar.swa_falcmmir_logic.enums.VoteDecision;
import swa.calendar.swa_falcmmir_logic.input_types.VoteInput;

@Entity
@Table(name = "vote")
@Getter
@Setter
public class VoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private long creatorId;

    @ManyToOne
    private SessionTimeEntity sessionTime;

    private String note;

    @Enumerated(EnumType.STRING)
    private VoteDecision decision;

    public VoteEntity() {
        this.decision = VoteDecision.NO;
    }

    public void setFromInput(VoteInput voteInput) {
        if (voteInput.getDecision() != null) {
            this.decision = voteInput.getDecision();
        }
        if (voteInput.getNote() != null) {
            this.note = voteInput.getNote();
        }
    }
}
