package swa.calendar.swa_falcmmir_logic.local_entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import swa.calendar.swa_falcmmir_logic.input_types.SessionTimeInput;

import java.time.LocalDateTime;

@Entity
@Table(name = "session_time")
@Getter
@Setter
public class SessionTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private GameSessionEntity gameSession;

    private LocalDateTime datetimeFrom;

    private LocalDateTime datetimeTo;

    private boolean notified;

    public void setFromInput(SessionTimeInput sessionTimeInput) {
        if (sessionTimeInput.getDatetimeFrom() != null) {
            this.datetimeFrom = sessionTimeInput.getDatetimeFrom();
        }
        if (sessionTimeInput.getDatetimeTo() != null) {
            this.datetimeTo = sessionTimeInput.getDatetimeTo();
        }
        if (sessionTimeInput.getNotified() != null) {
            this.notified = sessionTimeInput.getNotified();
        }
    }
}
