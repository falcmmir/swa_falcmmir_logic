package swa.calendar.swa_falcmmir_logic.context;

public class UserContext {
    private static final ThreadLocal<Long> userId = new ThreadLocal<>();

    public static Long getUserId() {
        return userId.get();
    }

    public static void setUserId(Long id) {
        userId.set(id);
    }

    public static void clear() {
        userId.remove();
    }
}
