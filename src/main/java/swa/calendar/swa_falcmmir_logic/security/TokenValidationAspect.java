package swa.calendar.swa_falcmmir_logic.security;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;
import swa.calendar.swa_falcmmir_logic.context.UserContext;
import swa.calendar.swa_falcmmir_logic.exceptions.UnauthorizedException;
import swa.calendar.swa_falcmmir_logic.services.AuthenticationService;

@Aspect
@Component
public class TokenValidationAspect {

    private final AuthenticationService authenticationService;

    @Autowired
    public TokenValidationAspect(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Before("@annotation(swa.calendar.swa_falcmmir_logic.security.TokenRequired) && args(token,..)")
    public void validateToken(JoinPoint joinPoint, @RequestHeader(HttpHeaders.AUTHORIZATION) String token) {
        // Validate the token (remove "Bearer " prefix if it is present)
        if (token.startsWith("Bearer ")) {
            token = token.substring(7);
        }

        // TODO: POKUD JE TO DOTAZ OD NOTIFICATION SERVICE, TAK return
        if (token.equals("PEPEGA")) {
            return;
        }

        // debugging
//        Long userId = 1L;
//        Long userId = null;

        Long userId = authenticationService.validateToken(token);

        // Check if the token validation failed
        if (userId == null) {
            throw new UnauthorizedException("Invalid or expired token");
        }

        UserContext.setUserId(userId);
    }
}
