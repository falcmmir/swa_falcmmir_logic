package swa.calendar.swa_falcmmir_logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaFalcmmirLogicApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaFalcmmirLogicApplication.class, args);
    }

}
