package swa.calendar.swa_falcmmir_logic.input_types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameSessionInput {
    private String name;
    private String description;
    private Integer minPlayers;
    private Integer maxPlayers;
    private Boolean single;
}
