package swa.calendar.swa_falcmmir_logic.input_types;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class SessionTimeInput {
    private LocalDateTime datetimeFrom;
    private LocalDateTime datetimeTo;
    private Boolean notified;
}
