package swa.calendar.swa_falcmmir_logic.input_types;

import lombok.Getter;
import lombok.Setter;
import swa.calendar.swa_falcmmir_logic.enums.VoteDecision;

@Getter
@Setter
public class VoteInput {
    private VoteDecision decision;
    private String note;
}
