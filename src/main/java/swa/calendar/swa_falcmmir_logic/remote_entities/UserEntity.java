package swa.calendar.swa_falcmmir_logic.remote_entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserEntity {
    private long id;

    private String name;
}
