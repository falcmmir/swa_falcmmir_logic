package swa.calendar.swa_falcmmir_logic.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import swa.calendar.swa_falcmmir_logic.exceptions.BadRequestException;
import swa.calendar.swa_falcmmir_logic.exceptions.ResourceNotFoundException;
import swa.calendar.swa_falcmmir_logic.input_types.SessionTimeInput;
import swa.calendar.swa_falcmmir_logic.local_entities.GameSessionEntity;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;
import swa.calendar.swa_falcmmir_logic.repositories.GameSessionRepository;
import swa.calendar.swa_falcmmir_logic.repositories.SessionTimeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SessionTimeService {
    private final SessionTimeRepository sessionTimeRepository;
    private final GameSessionRepository gameSessionRepository;

    @Autowired
    public SessionTimeService(SessionTimeRepository sessionTimeRepository, GameSessionRepository gameSessionRepository) {
        this.sessionTimeRepository = sessionTimeRepository;
        this.gameSessionRepository = gameSessionRepository;
    }

    @Transactional
    public SessionTimeEntity createSessionTime(Long gameSessionId, SessionTimeInput sessionTimeInput) {
        GameSessionEntity gameSession = gameSessionRepository.findById(gameSessionId)
                .orElseThrow(() -> new ResourceNotFoundException("GameSession not found with id: " + gameSessionId));

        SessionTimeEntity sessionTimeEntity = new SessionTimeEntity();
        sessionTimeEntity.setGameSession(gameSession);
        sessionTimeEntity.setFromInput(sessionTimeInput);
        return sessionTimeRepository.save(sessionTimeEntity);
    }

    @Transactional
    public SessionTimeEntity updateSessionTime(Long gameSessionId, Long sessionTimeId, SessionTimeInput sessionTimeInput) {
        if (!gameSessionRepository.existsById(gameSessionId)) {
            throw new ResourceNotFoundException("GameSession not found with id: " + gameSessionId);
        }
        SessionTimeEntity sessionTimeEntity = sessionTimeRepository.findById(sessionTimeId)
                .orElseThrow(() -> new ResourceNotFoundException("SessionTime not found with id: " + sessionTimeId));

        if (sessionTimeEntity.getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("SessionTime does not belong to the specified GameSession");
        }

        sessionTimeEntity.setFromInput(sessionTimeInput);
        return sessionTimeRepository.save(sessionTimeEntity);
    }

    public List<SessionTimeEntity> getAllSessionTimesForGameSession(Long gameSessionId) {
        return sessionTimeRepository.findAllByGameSessionId(gameSessionId);
    }

    public SessionTimeEntity getSessionTimeById(Long gameSessionId, Long sessionTimeId) {
        if (!gameSessionRepository.existsById(gameSessionId)) {
            throw new ResourceNotFoundException("GameSession not found with id: " + gameSessionId);
        }
        SessionTimeEntity sessionTimeEntity = sessionTimeRepository.findById(sessionTimeId)
                .orElseThrow(() -> new ResourceNotFoundException("SessionTime not found with id: " + sessionTimeId));

        if (sessionTimeEntity.getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("SessionTime does not belong to the specified GameSession");
        }

        return sessionTimeEntity;
    }

    @Transactional
    public void deleteSessionTime(Long gameSessionId, Long sessionTimeId) {
        if (!gameSessionRepository.existsById(gameSessionId)) {
            throw new ResourceNotFoundException("GameSession not found with id: " + gameSessionId);
        }
        SessionTimeEntity sessionTimeEntity = sessionTimeRepository.findById(sessionTimeId)
                .orElseThrow(() -> new ResourceNotFoundException("SessionTime not found with id: " + sessionTimeId));

        if (sessionTimeEntity.getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("SessionTime does not belong to the specified GameSession");
        }

        sessionTimeRepository.delete(sessionTimeEntity);
    }

    public List<SessionTimeEntity> filterSessionTimes(Long gameSessionId, SessionTimeInput sessionTimeInput) {
        return getAllSessionTimesForGameSession(gameSessionId).stream()
                .filter(sessionTime -> sessionTimeInput.getDatetimeFrom() == null ||
                        !sessionTime.getDatetimeFrom().isBefore(sessionTimeInput.getDatetimeFrom()))
                .filter(sessionTime -> sessionTimeInput.getDatetimeTo() == null ||
                        !sessionTime.getDatetimeTo().isAfter(sessionTimeInput.getDatetimeTo()))
                .filter(sessionTime -> sessionTimeInput.getNotified() == null ||
                        sessionTime.isNotified() == sessionTimeInput.getNotified())
                .collect(Collectors.toList());
    }
}
