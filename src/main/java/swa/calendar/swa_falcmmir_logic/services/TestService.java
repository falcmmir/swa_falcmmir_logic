package swa.calendar.swa_falcmmir_logic.services;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import swa.calendar.swa_falcmmir_logic.local_entities.GameSessionEntity;

import java.util.Map;

@Service
public class TestService {

    private final RestTemplate restTemplate;
    private final ServiceRegisterService serviceRegisterService;

    public TestService(RestTemplate restTemplate, ServiceRegisterService serviceRegisterService) {
        this.restTemplate = restTemplate;
        this.serviceRegisterService = serviceRegisterService;
    }

    public GameSessionEntity getGameSessionById(Long id) {
        String logicUrl = serviceRegisterService.getLogicUrl();

        String fullUrl = UriComponentsBuilder
                .fromUriString("http://" + logicUrl + "/game-sessions/" + id)
                .build().toUriString();

        return restTemplate.exchange(
                fullUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<GameSessionEntity>() {
                }
        ).getBody();
    }

    public Map<String, String> getNotificationHealth() {
        String notificationUrl = serviceRegisterService.getNotificationUrl();

        String fullUrl = UriComponentsBuilder
                .fromUriString("http://" + notificationUrl + "/actuators/health")
                .build().toUriString();

        return restTemplate.exchange(
                fullUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Map<String, String>>() {
                }
        ).getBody();
    }

    public Map<String, String> getAuthenticationHealth() {
        String authenticationUrl = serviceRegisterService.getAuthenticationUrl();

        String fullUrl = UriComponentsBuilder
                .fromUriString("http://" + authenticationUrl + "/health")
                .build().toUriString();

        return restTemplate.exchange(
                fullUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Map<String, String>>() {
                }
        ).getBody();
    }
}
