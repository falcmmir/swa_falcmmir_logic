package swa.calendar.swa_falcmmir_logic.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import swa.calendar.swa_falcmmir_logic.exceptions.BadRequestException;
import swa.calendar.swa_falcmmir_logic.exceptions.ResourceNotFoundException;
import swa.calendar.swa_falcmmir_logic.input_types.VoteInput;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;
import swa.calendar.swa_falcmmir_logic.local_entities.VoteEntity;
import swa.calendar.swa_falcmmir_logic.repositories.SessionTimeRepository;
import swa.calendar.swa_falcmmir_logic.repositories.VoteRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VoteService {
    private final VoteRepository voteRepository;
    private final SessionTimeRepository sessionTimeRepository;

    @Autowired
    public VoteService(VoteRepository voteRepository, SessionTimeRepository sessionTimeRepository) {
        this.voteRepository = voteRepository;
        this.sessionTimeRepository = sessionTimeRepository;
    }

    @Transactional
    public VoteEntity createVote(Long gameSessionId, Long sessionTimeId, VoteInput voteInput, Long creatorId) {
        SessionTimeEntity sessionTimeEntity = sessionTimeRepository.findById(sessionTimeId)
                .orElseThrow(() -> new ResourceNotFoundException("SessionTime not found with id: " + sessionTimeId));
        if (sessionTimeEntity.getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("SessionTime does not belong to the specified GameSession");
        }
        VoteEntity voteEntity = new VoteEntity();
        voteEntity.setSessionTime(sessionTimeEntity);
        voteEntity.setFromInput(voteInput);
        voteEntity.setCreatorId(creatorId);
        return voteRepository.save(voteEntity);
    }

    @Transactional
    public VoteEntity updateVote(Long gameSessionId, Long sessionTimeId, Long voteId, VoteInput voteInput, Long creatorId) {
        VoteEntity voteEntity = voteRepository.findById(voteId)
                .orElseThrow(() -> new ResourceNotFoundException("Vote not found with id: " + voteId));
        if (voteEntity.getSessionTime().getId() != sessionTimeId ||
                voteEntity.getSessionTime().getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("Vote does not belong to the specified SessionTime or GameSession");
        }
        voteEntity.setFromInput(voteInput);
        voteEntity.setCreatorId(creatorId);
        return voteRepository.save(voteEntity);
    }

    public List<VoteEntity> getAllVotes(Long gameSessionId, Long sessionTimeId) {
        return voteRepository.findBySessionTimeId(sessionTimeId).stream()
                .filter(vote -> vote.getSessionTime().getGameSession().getId() == gameSessionId)
                .collect(Collectors.toList());
    }

    public VoteEntity getVoteById(Long gameSessionId, Long sessionTimeId, Long voteId) {
        VoteEntity vote = voteRepository.findById(voteId)
                .orElseThrow(() -> new ResourceNotFoundException("Vote not found with id: " + voteId));
        if (vote.getSessionTime().getId() != sessionTimeId ||
                vote.getSessionTime().getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("Vote does not belong to the specified SessionTime or GameSession");
        }
        return vote;
    }

    @Transactional
    public void deleteVote(Long gameSessionId, Long sessionTimeId, Long voteId) {
        VoteEntity vote = voteRepository.findById(voteId)
                .orElseThrow(() -> new ResourceNotFoundException("Vote not found with id: " + voteId));
        if (vote.getSessionTime().getId() != sessionTimeId ||
                vote.getSessionTime().getGameSession().getId() != gameSessionId) {
            throw new BadRequestException("Vote does not belong to the specified SessionTime or GameSession");
        }
        voteRepository.delete(vote);
    }
}
