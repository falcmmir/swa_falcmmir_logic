package swa.calendar.swa_falcmmir_logic.services;


import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@Service
public class AuthenticationService {

    private final RestTemplate restTemplate;
    private final ServiceRegisterService serviceRegisterService;

    public AuthenticationService(RestTemplate restTemplate, ServiceRegisterService serviceRegisterService) {
        this.restTemplate = restTemplate;
        this.serviceRegisterService = serviceRegisterService;
    }

    public Long validateToken(String token) {
        String authenticationUrl = serviceRegisterService.getAuthenticationUrl();

        String fullUrl = UriComponentsBuilder
                .fromUriString("http://" + authenticationUrl + "/check")
                .build().toUriString();

        // Create headers and set the Authorization token
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);

        // Create the HTTP entity with the headers
        HttpEntity<String> requestEntity = new HttpEntity<>(headers);

        return restTemplate.exchange(
                fullUrl,
                HttpMethod.GET,
                requestEntity,
                new ParameterizedTypeReference<Long>() {
                }
        ).getBody();
    }
}
