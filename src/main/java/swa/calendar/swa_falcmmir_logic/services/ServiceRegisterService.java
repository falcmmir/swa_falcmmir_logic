package swa.calendar.swa_falcmmir_logic.services;


import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import swa.calendar.swa_falcmmir_logic.exceptions.InternalServerErrorException;


@Service
public class ServiceRegisterService {

    public static final String REGISTERED_SERVICES_URL = "http://service_register:80/services";
    public static final String NOTIFICATION_SERVICE_HOSTNAME = "notification";
    public static final String AUTHENTICATION_SERVICE_HOSTNAME = "authentication";
    public static final String LOGIC_SERVICE_HOSTNAME = "logic";

    private final RestTemplate restTemplate;

    public ServiceRegisterService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getNotificationUrl() {
        return getServiceUrl(NOTIFICATION_SERVICE_HOSTNAME);
    }

    public String getAuthenticationUrl() {
        return getServiceUrl(AUTHENTICATION_SERVICE_HOSTNAME);
    }

    public String getLogicUrl() {
        return getServiceUrl(LOGIC_SERVICE_HOSTNAME);
    }

    private String getServiceUrl(String serviceHostname) {
        String fullUrl = UriComponentsBuilder
                .fromUriString(REGISTERED_SERVICES_URL)
                .queryParam("type", serviceHostname)
                .toUriString();

        String serviceUrl = restTemplate.getForObject(fullUrl, String.class);

        if (serviceUrl == null) {
            throw new InternalServerErrorException(
                    "Service under hostname " + serviceHostname + " was not found by service register."
            );
        }

        return serviceUrl.replaceAll("\"", "");
    }
}
