package swa.calendar.swa_falcmmir_logic.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import swa.calendar.swa_falcmmir_logic.exceptions.ResourceNotFoundException;
import swa.calendar.swa_falcmmir_logic.input_types.GameSessionInput;
import swa.calendar.swa_falcmmir_logic.local_entities.GameSessionEntity;
import swa.calendar.swa_falcmmir_logic.repositories.GameSessionRepository;

import java.util.List;

@Service
public class GameSessionService {
    private final GameSessionRepository gameSessionRepository;

    @Autowired
    public GameSessionService(GameSessionRepository gameSessionRepository) {
        this.gameSessionRepository = gameSessionRepository;
    }

    @Transactional
    public GameSessionEntity createGameSession(GameSessionInput gameSessionInput, Long creatorId) {
        GameSessionEntity gameSessionEntity = new GameSessionEntity();
        gameSessionEntity.setFromInput(gameSessionInput);
        gameSessionEntity.setCreatorId(creatorId);
        return gameSessionRepository.save(gameSessionEntity);
    }

    @Transactional
    public GameSessionEntity updateGameSession(Long id, GameSessionInput gameSessionInput, Long creatorId) {
        GameSessionEntity gameSessionEntity = gameSessionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("GameSession not found with id: " + id));
        gameSessionEntity.setId(id);
        gameSessionEntity.setFromInput(gameSessionInput);
        gameSessionEntity.setCreatorId(creatorId);
        return gameSessionRepository.save(gameSessionEntity);
    }

    public List<GameSessionEntity> getAllGameSessions() {
        return gameSessionRepository.findAll();
    }

    public GameSessionEntity getGameSessionById(Long id) {
        return gameSessionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("GameSession not found with id: " + id));
    }

    @Transactional
    public void deleteGameSession(Long id) {
        GameSessionEntity existingGameSession = gameSessionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("GameSession not found with id: " + id));
        gameSessionRepository.delete(existingGameSession);
    }
}
