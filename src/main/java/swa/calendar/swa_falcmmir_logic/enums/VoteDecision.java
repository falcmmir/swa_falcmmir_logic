package swa.calendar.swa_falcmmir_logic.enums;

public enum VoteDecision {
    YES,
    NO,
    MAYBE
}
