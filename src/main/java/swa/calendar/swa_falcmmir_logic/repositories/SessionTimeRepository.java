package swa.calendar.swa_falcmmir_logic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import swa.calendar.swa_falcmmir_logic.local_entities.SessionTimeEntity;

import java.util.List;

@Repository
public interface SessionTimeRepository extends JpaRepository<SessionTimeEntity, Long> {
    List<SessionTimeEntity> findAllByGameSessionId(Long gameSessionId);
}
