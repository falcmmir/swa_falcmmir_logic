package swa.calendar.swa_falcmmir_logic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import swa.calendar.swa_falcmmir_logic.local_entities.VoteEntity;

import java.util.List;

@Repository
public interface VoteRepository extends JpaRepository<VoteEntity, Long> {
    List<VoteEntity> findBySessionTimeId(Long sessionTimeId);
}
